﻿using Entities.Models;
using System;

namespace JwtAuthentication
{
    public interface IJwtRepo
    {
        /// <summary>
        /// Creates jwt token used to authenticate a user
        /// </summary>
        string CreateJwtToken(Guid userId, string Role);
    }
}