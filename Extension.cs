﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace JwtAuthentication
{
    public static class Extensions
    {
        public static void ConfigureJwtAuthentication(this IServiceCollection services,
            IConfiguration config)
        {
            var jwtSettings = config.GetSection("JwtSettings").Get<JwtSettingsDto>();

            var key = Encoding.ASCII.GetBytes(jwtSettings.Secret.ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(c =>
            {
                c.RequireHttpsMetadata = false;
                c.SaveToken = true;
                c.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),

                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,

                    //ValidIssuer = jwtSettings.Issuer,
                    //ValidAudience = jwtSettings.Audience
                };
            });
        }
    }
}