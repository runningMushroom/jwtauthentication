﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JwtAuthentication
{
    public class JwtSettingsDto
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int LifeSpan { get; set; }
    }
}